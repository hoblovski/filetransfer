#include "msg.h"

#include "common.h"

struct msg_t* read_msg(struct rio_struct* rio, char* buf)
{
    struct msg_t* msg = (struct msg_t*) buf;
    if (rio_read(rio, buf, MSG_HEADER_SZ) != 0)
        return NULL;
//    msg->type = ntohl(msg->type);
//    msg->len = ntohl(msg->len);
    if (msg->len < 8)
        Fatal("read_msg: broken msg->len from client.");
    if (rio_read(rio, buf + MSG_HEADER_SZ, msg->len - MSG_HEADER_SZ)
            != 0)
        return NULL;
    buf[msg->len] = 0; // terminating zero for strings
    return msg;
}

int write_msg(struct rio_struct* rio, struct msg_t* msg)
{
    char* buf = (char*) msg;
//    msg->type = htonl(msg->type);
//    msg->len = htonl(msg->len);
    if (rio_write(rio, buf, msg->len) != 0)
        return -1;
    return 0;
}
