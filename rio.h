#ifndef RIO_H
#define RIO_H

#include <unistd.h>

/* robust IO wrapper for unix std IO.
 *
 * Works on Linux (syscalls interrupted will be restarted)
 *  (i.e. *** There is no check for EINTR ***)
 *
 * The name 'rio' is borrowed from csapp.
 *
 * Feature:
 *  - Buffered -> reduce expensive syscalls
 *  - Thread safe -> use rio_struct
 *  - Robust -> especially used for sockets
 *
 * Timeout mechanism is implemented using setsocksopt elsewhere, not in rio.
 * @Author: Hoblovski 2018.3
 */

#define RIO_BUF_SIZE 1048576
struct rio_struct {
    // read buffers
    char buf[RIO_BUF_SIZE];
    char* beg;
    char* end;

    // one rio for one fd, please
    int fd;
};

void rio_init(struct rio_struct* rio, int fd);
/*
 * @count: read exactly count bytes. blocking call.
 *
 * @returns: either 0 (read exactly count bytes) 
 *           or -1 (error, or insufficient remaining input)
 */
ssize_t rio_read(struct rio_struct* rio, void* buf, size_t count);

/*
 * @returns: either 0 or -1, same as rio_read
 */
ssize_t rio_write(struct rio_struct* rio, const void* buf, size_t count);

int rio_close(struct rio_struct* rio);

#endif // RIO_H
