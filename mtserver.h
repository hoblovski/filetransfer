#ifndef MTSERVER_H
#define MTSERVER_H

/* A minimalist multithreaded server framework
 */

int mtserver_setup(int port, int backlog);

/*
 * Obviously we do not employ thread pooling.
 * Because this is a * minimalist * design!
 *
 * @connection_handler: returns void, arg=connfd.
 * @stdin_handler: returns void, arg=connfd.
 */
void mtserver_mainloop(int sockfd,
        void* (*connection_handler)(void*),
        void* (*stdin_handler)(void*));

#endif // MTSERVER_H
