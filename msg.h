#ifndef MSG_H
#define MSG_H

#include "rio.h"

/*
 * message format:
 *  msg_type (4 bytes)
 *  msg_length (4 bytes, including header, in bytes)
 *  msg_data ...
 */

#define MAX_MSG_LEN 65536
#define MSG_HEADER_SZ 8

#define MSG_TYPE_CONTENT_REQUEST 0x10
#define MSG_TYPE_CONTENT_RESPONSE 0x11
#define MSG_TYPE_CONTENT_RESPONSE_LAST 0x13
#define MSG_TYPE_CONTENT_RESPONSE_ERR 0x12
#define MSG_TYPE_CONTENT_RESPONSE_BADREQ 0x14
struct msg_t {
    int type;
    int len;
    char data;
};

/*
 * read a single message.
 *  I don't do byte order swapping. (ntoh hton)
 *
 * @buf: at least MAX_MSG_LEN
 *
 * @returns: NULL or ((msg_t*) buf
 */
struct msg_t* read_msg(struct rio_struct* rio, char* buf);

/*
 * @returns: -1 (error) or 0 (all well)
 */
int write_msg(struct rio_struct* rio, struct msg_t* msg);

#endif // MSG_H
