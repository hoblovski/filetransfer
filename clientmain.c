#include <stdio.h>
#include <unistd.h>

#include "common.h"
#include "rio.h"
#include "client.h"
#include "msg.h"

static char localhost[] = "127.0.0.1";

/*
 * clientmain is single threaded so we don't worry about races.
 *
 * Therefore static variables in functions are safe.
 */
int main(int argc, char** argv)
{
    if (argc != 3 && argc != 4) {
        printf("Usage: %s [SERVER_ADDRESS] SERVER_PORT NAME\n", argv[0]);
        printf("\nSERVER_ADDRESS: defaults to localhost\n");
        printf("NAME: what resource do you want?\n");
        exit(0);
    }
    printf("client...\n");

    // parsing command line args
    char* srvaddr;;
    int srvport;
    char* resname;
    if (argc == 3) {
        srvaddr = localhost;
        srvport = atoi(argv[1]);
        resname = argv[2];
    } else {
        srvaddr = argv[1];
        srvport = atoi(argv[2]);
        resname = argv[3];
    }
    printf("fetching %s from %s:%d\n", resname, srvaddr, srvport);

    static char buf[MAX_MSG_LEN];
    struct msg_t* msg = (struct msg_t*) buf;
    char* dataptr = buf + MSG_HEADER_SZ;

    // construct message
    msg->type = MSG_TYPE_CONTENT_REQUEST;
    msg->len = MSG_HEADER_SZ + strlen(resname);
    // content request should always be contained in one message
    if (msg->len > MAX_MSG_LEN)
        Fatal("name of requested file is too long.");
    strcpy(dataptr, resname); // strcpy guaranteed to be safe here

    // send request
    int connfd = client_setup(srvaddr, srvport);
    static struct rio_struct rio;
    rio_init(&rio, connfd);

    write_msg(&rio, msg);

    // receive response
    FILE* fout = fopen(resname, "wb");
    while (1) {
        msg = read_msg(&rio, buf);
        if (msg->type == MSG_TYPE_CONTENT_RESPONSE_ERR) {
            printf("server internal error.\n");
            goto cleanup;
        }
        if (msg->type == MSG_TYPE_CONTENT_RESPONSE_BADREQ) {
            printf("request denied by server.\n");
            printf("check if NAME contains illegal characters.\n");
            goto cleanup;
        }
        if (msg->type != MSG_TYPE_CONTENT_RESPONSE_LAST
                && msg->type != MSG_TYPE_CONTENT_RESPONSE) {
            printf("unknown response type from server.\n");
            goto cleanup;
        }
        fwrite(dataptr, 1, msg->len - MSG_HEADER_SZ, fout);
        if (msg->type == MSG_TYPE_CONTENT_RESPONSE_LAST) {
            printf("complete! outputs written to %s\n", resname);
            break;
        } else {
            putchar('.'); // something like the progress bar
            // to tell user that we are not dead
        }
    }

cleanup:
    fclose(fout);
    rio_close(&rio);
    return 0;
}

