#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#include "common.h"
#include "rio.h"
#include "mtserver.h"
#include "msg.h"

#define MAX_PATH_LEN 1024

#define ERR_FILE_NOT_FOUND 1
#define ERR_UNKNOWN 2

char workingdir[MAX_PATH_LEN];

void* stdin_handler(void*);
void* conn_handler(void*);
int serve_file(struct rio_struct* rio, char* file_path, char* buf);

/*
 * For securirty reasons, the requested resource name cannot contain '.'
 *  character.
 */
int main(int argc, char** argv)
{
    if (argc != 2) {
        printf("Usage: %s PORT\n", argv[0]);
        exit(0);
    }

    if (getcwd(workingdir, sizeof(workingdir)) == NULL)
        Fatal("working directory too deep!\n");
    printf("server...\n");
    printf("working on %s\n", workingdir);

    int sockfd = mtserver_setup(atoi(argv[1]), 2);
    mtserver_mainloop(sockfd, &conn_handler, &stdin_handler);
}

// there should be only one thread running stdin_handler
void* stdin_handler(void* dummy) 
{
    static char buf[1024];          // static is thread safe here
    memset(buf, 0, sizeof(buf));

    while (1) {
        fgets(buf, 1024, stdin);
        if (!strcmp(buf, "quit\n")) {
            exit(0);
        } else {
            printf("unrecognized command!\n");
        }
    }

    Fatal("servermain.c: stdin_handler exits!");
    return NULL; // never here
}

/*
 * I intend to use one connection for each request.
 */
void* conn_handler(void* _connfd) 
{
    char* buf = malloc(MAX_MSG_LEN);
    assert(buf != NULL);
    char* file_path = malloc(MAX_PATH_LEN);
    assert(file_path != NULL);

    int connfd = (int) _connfd;
    struct rio_struct* rio = (struct rio_struct*)
            malloc(sizeof(struct rio_struct));
    assert(rio != NULL);
    rio_init(rio, connfd);
    struct msg_t* msg = read_msg(rio, buf);
    if (msg == NULL) {
        goto cleanup;
    }
    if (msg->type != MSG_TYPE_CONTENT_REQUEST) {
        printf("conn_handler: unknown msg type 0x%02x", msg->type);
        goto cleanup;
    } else {
        if (strchr((char*) &(msg->data), '.') != NULL) {
            printf("conn_handler: requested res name contains '.'");
            struct msg_t msg;
            msg.type = MSG_TYPE_CONTENT_RESPONSE_BADREQ;
            msg.len = 8;
            if (write_msg(rio, &msg) != 0)
                goto cleanup;
            goto cleanup;
        }
    }

    snprintf(file_path, MAX_PATH_LEN, "%s/%s", workingdir,
            (char*) &(msg->data));

    printf("serving %s\n", file_path);
    if (serve_file(rio, file_path, buf) != 0) {
        printf("serve %s failed.\n", file_path);
        goto cleanup;
    }
    printf("served.\n");

cleanup:
    rio_close(rio);
    free(buf);
    free(file_path);
    free(rio);
    return NULL;
}

int serve_file(struct rio_struct* rio, char* file_path, char* buf)
{
    struct msg_t* msg = (struct msg_t*) buf;
    char* ptr = buf + MSG_HEADER_SZ;
    char* end = buf + MAX_MSG_LEN;
    int retval = ERR_UNKNOWN;

    // use stdio because they offer buffering
    FILE* fin = fopen(file_path, "rb");
    if (fin == NULL) {
        printf("serve_file: non-existent file %s\n", file_path);
        return ERR_FILE_NOT_FOUND;
    }

    msg->type = MSG_TYPE_CONTENT_RESPONSE;
    while (1) {
        // file too large to fie in one message
        if (end - ptr == 0) { 
            msg->len = ptr - buf;
            if (write_msg(rio, msg) != 0)
                goto cleanup;
            printf(".");
            ptr = buf + MSG_HEADER_SZ;
            continue;
        }
        int n_read = fread(ptr, 1, end - ptr, fin);
        ptr += n_read;
        if (n_read != 0) continue; 
        // end of file
        if (feof(fin))  {
            // no more messages
            msg->type = MSG_TYPE_CONTENT_RESPONSE_LAST;
            msg->len = ptr - buf;
            if (write_msg(rio, msg) != 0)
                goto cleanup;
            printf(".\n");
            retval = 0;
            goto cleanup;
        }
        if (ferror(fin)) {
            msg->type = MSG_TYPE_CONTENT_RESPONSE_ERR;
            msg->len = MSG_HEADER_SZ;
            if (write_msg(rio, msg) != 0)
                goto cleanup;
            printf(".\n");
            printf("serve_file: unknown IO error of %s\n", file_path);
            goto cleanup;
        }
    }

cleanup:
    fclose(fin);
    return retval;
}
