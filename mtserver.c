#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include <signal.h>
#include <errno.h>

#include "common.h"
#include "mtserver.h"

static void sigpipe_handler(int _sigtype)
{
    if (_sigtype != SIGPIPE)
        Fatal("sigpipe_handler: bad sigtype. have you written wrong");
    printf("SIGPIPE received, ignored.\n");
}


int mtserver_setup(int port, int backlog) 
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == 0)
        Fatal("mtserver_setup: create socket falied.");

    // put reuse addr/port before bind
    int enable = 1;
    if (setsockopt(sockfd, SOL_SOCKET,
                SO_REUSEADDR, &enable, sizeof(int)) < 0)
        Fatal("mtserver_setup: setsockopt SO_REUSEADDR failed");
    if (setsockopt(sockfd, SOL_SOCKET,
                SO_REUSEPORT, &enable, sizeof(int)) < 0)
        Fatal("mtserver_setup: setsockopt SO_REUSEPORT failed");

    // ignore SIG_PIPE to not halt on writing to closed sockets
    signal(SIGPIPE, sigpipe_handler);

    // set SO_LINGER to prevent FIN-WAIT2 attakcs
    struct linger lg = {
        /*l_onoff=*/1,
        /*l_linger=*/0
    };
    if (setsockopt(sockfd, SOL_SOCKET, SO_LINGER, &lg, sizeof(lg)) < 0)
        Fatal("mtserver_setup: setsockopt SO_LINGER failed");

    // set send/recv timeout
    struct timeval timeout = {
        /*secs=*/1, /*usecs=*/0
    };
    if (setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, &timeout,
                sizeof(timeout)) < 0)
        Fatal("mtserver_setup: setsockopt SO_SNDTIMEO failed");
    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &timeout,
                sizeof(timeout)) < 0)
        Fatal("mtserver_setup: setsockopt SO_RCVTIMEO failed");

    // no-delay, make server faster
    if (setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY,
                &enable, sizeof(enable)) < 0)
        Fatal("mtserver_setup: setsockopt TCP_NODELAY failed");

    struct sockaddr_in srvaddr;
    sockaddr_create(&srvaddr, INADDR_ANY, port);
    if (bind(sockfd, (struct sockaddr*) &srvaddr, sizeof(srvaddr)) < 0)
        Fatal("mtserver_setup: bind failed");
    if (listen(sockfd, backlog) != 0)
        Fatal("mtserver_setup: listen failed");
    printf("mtserver_setup: success, listening on port %d\n", port);
    return sockfd;
}


void mtserver_mainloop(int sockfd,
        void* (*connection_handler)(void*),
        void* (*stdin_handler)(void*))
{
    int connfd;
    int cliaddr_sz = sizeof(struct sockaddr_in);
    struct sockaddr_in cliaddr;

    pthread_t stdin_thread;

    if (pthread_create(&stdin_thread, NULL, stdin_handler, NULL) < 0)
        Fatal("mtserver_mainloop: failed to create stdin_thread");

    while ((connfd = accept(sockfd, (struct sockaddr*) &cliaddr,
                    (socklen_t*) &cliaddr_sz))) {
        if (connfd == -1) {
            if (errno == EAGAIN) continue;
            printf("mtserver_mainloop: ignored error on accept. [errno=%d]\n",
                    errno);
            continue;
        }

        printf("connection accepted from %s:%d\n",
                inet_ntoa(cliaddr.sin_addr), cliaddr.sin_port);

        pthread_t worker;

        if (pthread_create(&worker, NULL,
                connection_handler, (void*) connfd) < 0)
            Fatal("mtserver_mainloop: failed to create worker");

        if (pthread_detach(worker) != 0)
            Fatal("mtserver_mainloop: failed to detach worker");
    }
}

