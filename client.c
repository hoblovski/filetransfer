#include "common.h"
#include <assert.h>

int client_setup(char* srvip, int srvport)
{
    /* create fd */
    int sockfd;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        Fatal("client_setup: create socket failed");

    /* create addr */
    struct sockaddr_in serv_addr;
    sockaddr_create_ips(&serv_addr, srvip, srvport);

    /* connect */
    if (connect(sockfd, (struct sockaddr*) &serv_addr,
                sizeof(serv_addr)) < 0)
        Fatal("client_setup: failed to connect to server");
    return sockfd;
}


