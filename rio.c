#include "rio.h"

#include <string.h>
#include <errno.h>

static inline int min(int a, int b) {
    return a < b ? a : b;
}

static inline void rio_advance_beg(struct rio_struct* rio, int n) {
    rio->beg = rio->buf + ((rio->beg - rio->buf + n) % RIO_BUF_SIZE);
}

static inline void rio_advance_end(struct rio_struct* rio, int n) {
    rio->end = rio->buf + ((rio->end - rio->buf + n) % RIO_BUF_SIZE);
}

static inline int rio_size(struct rio_struct* rio) {
    int t = (rio->end - rio->beg);
    return (t < 0) ? (t + RIO_BUF_SIZE) : t;
}

void rio_init(struct rio_struct* rio, int fd)
{
    rio->beg = rio->buf;
    rio->end = rio->buf;
    rio->fd = fd;
}

/*
 * There is buffer for read.
 */
ssize_t rio_read(struct rio_struct* rio, void* buf, size_t count)
{
    char* dst = buf;
    while (count > 0) {
        int n_read = min(count, rio_size(rio));
        if (n_read == 0) {
            // no remaining input in buffer
            rio->beg = rio->buf;
            rio->end = rio->buf;
            // each read syscall uses maximum size,
            // so the number of read's is minimalized
            // watch out for the "-1" here, it's a must!!!
            n_read = read(rio->fd, rio->beg, RIO_BUF_SIZE-1);
            // even if n_read == -1 is caused by read timeout,
            //  we will ignore the input because timeout is
            //  sufficiently large, so long timeouts are
            //  not tolerated.
            if (n_read < 0)
                return -1;
            if (n_read == 0)
                return -1;
            rio_advance_end(rio, n_read);
        } else {
            // remaining input in buffer, copy them first
            if (rio->beg > rio->end 
                    && n_read > (rio->buf + RIO_BUF_SIZE - rio->beg)) {
                int t = rio->buf + RIO_BUF_SIZE - rio->beg;
                memcpy(dst, rio->beg, t);
                memcpy(dst + t, rio->buf, n_read - t);
            } else {
                memcpy(dst, rio->beg, n_read);
            }
            dst += n_read;
            rio_advance_beg(rio, n_read);
            count -= n_read;
        }
    }
    return 0;
}

/* 
 * There's no buffer for write
 */
ssize_t rio_write(struct rio_struct* rio, const void* buf, size_t count)
{
    const char* src = buf;
    while (count > 0) {
        int n_write = write(rio->fd, src, count);
        if (n_write < 0) 
            return -1;
        src += n_write;
        count -= n_write;
    }
    return 0;
}

int rio_close(struct rio_struct* rio)
{
    int t = close(rio->fd);
    return t;
}
